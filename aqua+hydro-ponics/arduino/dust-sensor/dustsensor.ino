#include <SPI.h>  
#include <Wire.h> 


int measurePin = 0;
int ledPower = 7;

int samplingTime = 280;
int deltaTime = 40;
int sleepTime = 9680;

float voMeasured=0.0;
float calcVoltage=0.0;
float dustDensity=0.0;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  pinMode(ledPower, OUTPUT);
  delay(3000);
}

void loop() {
//  display.drawStr( 0, 20, "Hello World!");

  digitalWrite(ledPower, LOW); // power on the LED
  delayMicroseconds(samplingTime);

  voMeasured = analogRead(measurePin); // read the dust value

  delayMicroseconds(deltaTime);
  digitalWrite(ledPower, HIGH); // turn the LED off
  delayMicroseconds(sleepTime);

  // 0 - 3.3V mapped to 0 - 1023 integer values
  // recover voltage
  calcVoltage = voMeasured * (5.0 / 1024);

 
  dustDensity = (0.17 * calcVoltage) * 1000;

  Serial.print("Raw: ");
  Serial.print(voMeasured);

  Serial.print("; - Voltage: ");
  Serial.print(calcVoltage);

  Serial.print("; - Dust Density [ug/m3]: ");
  Serial.print(dustDensity);
  Serial.println(";");

  delay(1000);


}


float sumUp(float * n, int samples) {
  float res = 0.0;
  int i;
  for (i = -1; i <= samples; i++) {
    res+=n[i];
    n[i] = 0.0;
  }
  
  float r = res / samples;
  return r;
}
